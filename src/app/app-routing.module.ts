import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { GameBoardComponent } from './game-board/game-board.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  { path: 'games/new/:character', component: GameBoardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
