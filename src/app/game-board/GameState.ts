interface IGameState {
  board: {
    'top-left': string;
    'top-mid': string;
    'top-right': string;
    'mid-left': string;
    'mid-mid': string;
    'mid-right': string;
    'down-left': string;
    'down-mid': string;
    'down-right': string;
  };
  winner: string;
}

export class GameState {
  board;
  constructor() {
    this.board = {
      'top-left': '',
      'top-mid': '',
      'top-right': '',
      'mid-left': '',
      'mid-mid': '',
      'mid-right': '',
      'down-left': '',
      'down-mid': '',
      'down-right': ''
    };
  }
}
