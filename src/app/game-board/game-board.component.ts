import { Component, OnInit } from '@angular/core';
import { GameState } from './GameState';
import { NotificationService } from '../shared/notification.service';
import { browser } from 'protractor';

const squares = [
  'top-left',
  'top-mid',
  'top-right',
  'mid-left',
  'mid-mid',
  'mid-right',
  'down-left',
  'down-mid',
  'down-right'
];

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.css']
})
export class GameBoardComponent implements OnInit {
  userCharacter: string;
  actionOn: string;
  singlePlayer: boolean;
  inProgress: boolean;
  state;

  constructor(public notify: NotificationService) {}

  ngOnInit() {
    const match = document.location.pathname.match(/\/games\/new\/(\w+)$/);

    if (match) {
      const userCharacter = match[1];
      console.log('the user has choosen ', userCharacter);
      this.userCharacter = userCharacter;
    }
    this.actionOn = this.userCharacter;
    this.singlePlayer = true;
    this.inProgress = true;
    this.state = new GameState();
  }

  /**
   * Called when the user clicks a square to take their turn
   */
  userMove(event) {
    const userSelection = document.getElementById(event.target.id);
    const userImage = this.getCharacterImage(this.userCharacter);
    const image = document.createElement('IMG');
    image.setAttribute('src', userImage);
    image.setAttribute('alt', 'A flat monster icon');
    image.setAttribute('width', '128');
    image.setAttribute('height', '128');
    userSelection.appendChild(image);
    // remove the square from the list of available squares
    this.removeSquareFromArr(event.target.id);
    // update the GameState with the square:character
    this.state.board[event.target.id] = this.userCharacter;
    // -- check for winner -- //
    this.checkForWinner();
    this.moveActionForward();
  }

  /**
   * Called after a user takes their turn in single player mode
   */
  computerMove() {
    const len = squares.length;
    const index = Math.floor(Math.random() * len);

    //
    const image = document.createElement('IMG');
    image.setAttribute('src', 'assets/characters/godzilla.png');
    image.setAttribute('alt', 'A flat monster icon');
    image.setAttribute('width', '128');
    image.setAttribute('height', '128');
    const square = squares[index];
    const squareLocation = document.getElementById(square);
    squareLocation.appendChild(image);
    // remove the square from the list of available squares
    this.removeSquareFromArr(square);
    // update the GameState with the square:character
    this.state.board[square] = 'godzilla';
    // -- check for winner -- //
    this.checkForWinner();
    this.moveActionForward();
  }

  /**
   * Given a character name, return the appropriate image
   */
  private getCharacterImage(characterName) {
    const imgObj = {
      aaargh: '/assets/characters/aaargh.png',
      cyclops: '/assets/characters/cyclops.png',
      frank: '/assets/characters/frank.png',
      happy: '/assets/characters/happy.png',
      octopus: '/assets/characters/octopus.png',
      pinky: '/assets/characters/pinky.png',
      smurf: '/assets/characters/smurf.png'
    };

    return imgObj[characterName];
  }

  /**
   * Remove the occupied square from the list of available squares
   */
  private removeSquareFromArr(square: string) {
    const index = squares.indexOf(square);
    squares.splice(index, 1);
  }

  /**
   * Move the action to the next player or computer
   */
  private moveActionForward() {
    if (this.actionOn !== 'Computer') {
      this.actionOn = 'Computer';
      if (this.singlePlayer && this.inProgress) {
        this.computerMove();
      }
    } else {
      this.actionOn = this.userCharacter;
    }
  }

  private checkForWinner() {
    console.log('checking for winner');
    console.log(this.state);
    // winner top row
    if (
      this.state.board['top-left'] === this.userCharacter &&
      this.state.board['top-mid'] === this.userCharacter &&
      this.state.board['top-right'] === this.userCharacter
    ) {
      this.gameOver();
    }
    // winner middle row
    if (
      this.state.board['mid-left'] === this.userCharacter &&
      this.state.board['mid-mid'] === this.userCharacter &&
      this.state.board['mid-right'] === this.userCharacter
    ) {
      this.gameOver();
    }
    // winner bottom row
    if (
      this.state.board['down-left'] === this.userCharacter &&
      this.state.board['down-mid'] === this.userCharacter &&
      this.state.board['down-right'] === this.userCharacter
    ) {
      this.gameOver();
    }
    // winner left column
    if (
      this.state.board['top-left'] === this.userCharacter &&
      this.state.board['mid-left'] === this.userCharacter &&
      this.state.board['down-left'] === this.userCharacter
    ) {
      this.gameOver();
    }

    // winner middle column
    if (
      this.state.board['top-mid'] === this.userCharacter &&
      this.state.board['mid-mid'] === this.userCharacter &&
      this.state.board['down-mid'] === this.userCharacter
    ) {
      this.gameOver();
    }
    // winner right column
    if (
      this.state.board['top-right'] === this.userCharacter &&
      this.state.board['mid-right'] === this.userCharacter &&
      this.state.board['down-right'] === this.userCharacter
    ) {
      this.gameOver();
    }
    // winner diagnal top left to bottom right
    if (
      this.state.board['top-left'] === this.userCharacter &&
      this.state.board['mid-mid'] === this.userCharacter &&
      this.state.board['down-right'] === this.userCharacter
    ) {
      this.gameOver();
    }
    // winner diagnal bottom left to top right
    if (
      this.state.board['down-left'] === this.userCharacter &&
      this.state.board['mid-mid'] === this.userCharacter &&
      this.state.board['top-right'] === this.userCharacter
    ) {
      this.gameOver();
    }
  }

  gameOver() {
    this.inProgress = false;
    this.playAudio();
  }
  playAgain() {
    location.reload();
  }

  playAudio() {
    const audio = new Audio();
    audio.src = 'assets/tada.mp3';
    audio.load();
    audio.play();
  }
}
